package es.vidal;

import java.io.*;
import java.util.ArrayList;

public class Accounter {
    public static void main(String[] args) throws IOException {

        String adder = "out/artifacts/Ejercicio5Adder_jar/Ejercicio5Adder.jar";
        String totals = "resources/totals.txt";
        int sumaTotal = 0;


        File fichero1 = new File("resources/fichero1.txt");
        File fichero2 = new File("resources/fichero2.txt");
        File fichero3 = new File("resources/fichero3.txt");
        File totalsFile = new File(totals);

        ArrayList<File> ficheros = new ArrayList<>();
        ficheros.add(fichero1);
        ficheros.add(fichero2);
        ficheros.add(fichero3);

        if (!totalsFile.exists()){
            totalsFile.createNewFile();
        }

        try (FileWriter fileWriter = new FileWriter(totalsFile);
             BufferedWriter bufferedWriterDestino = new BufferedWriter(fileWriter)){
            for (int i = 0; i < ficheros.size(); i++) {
                int total = obtenerTotal(ficheros.get(i), adder);
                sumaTotal += total;
                bufferedWriterDestino.write("Total del fichero" + (i + 1) + " = " + total);
                bufferedWriterDestino.newLine();
                bufferedWriterDestino.flush();
            }

            bufferedWriterDestino.write("La suma total de todos los archivos es = " + sumaTotal);
            bufferedWriterDestino.newLine();
            bufferedWriterDestino.flush();
        }catch (IOException e){
            System.err.println(3);
            System.out.println(e.getMessage());
        }
    }

    /***
     * Este metodo se encarga de abrir un proceso hijo para mediante este conseguir la suma total de todos los enteros
     * de dicho fichero
     * @param file Fichero del cual queremos sumar los valores
     * @param  adder Ruta del .jar para iniciar el proceso hijo
     * @throws IOException IOException que puede ser lanzada por los diferentes búffers
     */
    private static int obtenerTotal(File file, String adder) throws  IOException{
        Process process = new ProcessBuilder("java", "-jar", adder, file.toString()).start();
        String total = "";

        try (InputStreamReader inputStreamReader = new InputStreamReader(process.getInputStream());
             BufferedReader bufferedReader = new BufferedReader(inputStreamReader)){

             total = bufferedReader.readLine();

            processWaitFor(process);
        }catch (IOException | InterruptedException e){
            System.err.println(2);
            System.out.println(e.getMessage());
        }
        return Integer.parseInt(total);
    }

    /***
     * Este metodo se encarga de verificar que todo ha salido correctamente
     * @param process Proceso hijo
     * @throws InterruptedException Excepción lanzada si se interrumpe el proceso
     */
    private static void processWaitFor(Process process) throws InterruptedException {
        int exitValue = process.waitFor();
        System.out.println(System.lineSeparator() + "Valor de Salida: " + exitValue);
        if (exitValue != 0) {
            System.err.println("ERROR DE SUMA");
            printChildError(process.getErrorStream());
        }
    }

    /***
     * Este metodo recibe un proceso y lee el errorStream para seguidamente leerlo por pantalla
     * @param errorStream es el ErrorStream del proceso hijo
     */
    private static void printChildError(InputStream errorStream) {
        String line;
        try (BufferedReader br = new BufferedReader(new InputStreamReader(errorStream))){
            while ((line = br.readLine()) != null) {
                System.out.println(line);
            }
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }
    }
}
