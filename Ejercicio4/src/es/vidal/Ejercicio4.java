package es.vidal;

import java.io.*;

public class Ejercicio4 {

    final static String TRADUCCIONES_FILE = "resources/traducciones.txt";

    public static void main(String[] args) throws IOException, InterruptedException {
        final String traductor = "out/artifacts/Ejercicio4Traductor_jar/Ejercicio4Traductor.jar";

        File traducciones = new File(TRADUCCIONES_FILE);
        Process process = new ProcessBuilder("java", "-jar", traductor).start();
        
        if (!traducciones.exists()){
            traducciones.createNewFile();
        }
        if (args.length > 0 && !args[0].contains("diccionario.txt")){
            comunicacionPadreHijoFichero(process, traducciones, args[0]);
        }else {
            if (args[0].contains("diccionario.txt")) {
                comunicacionPadreHijoDiccionario(process, traducciones, args[0]);
            }else {
                comunicacionPadreHijo(process, traducciones);
            }
        }
        processWaitFor(process);
    }

    /***
     * Este metodo recibe un proceso y lee el errorStream para seguidamente leerlo por pantalla
     * @param errorStream es el ErrorStream del proceso hijo
     */
    private static void printChildError(InputStream errorStream) {
        String line;
        try (BufferedReader br = new BufferedReader(new InputStreamReader(errorStream))){
            while ((line = br.readLine()) != null) {
                System.out.println(line);
            }
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }
    }

    /***
     * Este metodo se encarga de realizar la comunicación entre el proceso padre e hijo mediante un fichero para
     * traducir pasado por argumento
     * @param process Proceso hijo
     * @param traducciones Fichero destino de las traducciones
     * @param fichero fcihero a traducir
     */
    private static void comunicacionPadreHijoFichero(Process process, File traducciones, String fichero){

        File file = new File(fichero);

        try (OutputStreamWriter outputStreamWriter = new OutputStreamWriter(process.getOutputStream());
             InputStreamReader inputStreamReader = new InputStreamReader(process.getInputStream());
             BufferedReader bufReader = new BufferedReader(inputStreamReader);
             BufferedWriter bufWriter = new BufferedWriter(outputStreamWriter);
             FileWriter fileWriter = new FileWriter(traducciones);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
             FileReader fileReader = new FileReader(file);
             BufferedReader bufferedReader = new BufferedReader(fileReader)){

            String line = bufferedReader.readLine();
            while ( line != null){

                bufWriter.write(line);
                bufWriter.newLine();
                bufWriter.flush();

                String traduccion = bufReader.readLine();
                System.out.println(traduccion);
                bufferedWriter.write(line + " -> " + traduccion);
                bufferedWriter.newLine();
                bufferedWriter.flush();
                line = bufferedReader.readLine();

            }
        } catch (IOException e){
            System.err.println(1);
            System.out.println(e.getMessage());
        }
    }

    /***
     * Este metodo se encarga de realizar la comunicación entre el proceso padre e hijo sin argumentos
     * @param process Proceso hijo
     * @param traducciones Fichero destino de las traducciones
     */
    private static void comunicacionPadreHijo(Process process, File traducciones) {
        try (OutputStreamWriter outputStreamWriter = new OutputStreamWriter(process.getOutputStream());
             InputStreamReader inputStreamReader = new InputStreamReader(process.getInputStream());
             BufferedReader bufReader = new BufferedReader(inputStreamReader);
             BufferedReader bufReadCommLine = new BufferedReader(new InputStreamReader(System.in));
             BufferedWriter bufWriter = new BufferedWriter(outputStreamWriter);
             FileWriter fileWriter = new FileWriter(traducciones);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {

            buffers(bufReader, bufReadCommLine, bufWriter, bufferedWriter);
        } catch (IOException e){
            System.err.println(1);
            System.out.println(e.getMessage());
        }
    }

    /***
     * Este metodo se encarga de realizar la comunicación entre el proceso padre e hijo usando los búffers
     * @param bufferedWriter buffered writer conectado al proceso hijo
     * @param bufWriter buffered writer conectado al fichero destino
     * @param bufReadCommLine buffered reader conectado al terminal del sistema
     * @param bufReader buffered reader conectado al proceso hijo
     * @throws IOException IOexception por los búffers
     */
    private static void buffers(BufferedReader bufReader, BufferedReader bufReadCommLine, BufferedWriter bufWriter
            , BufferedWriter bufferedWriter) throws IOException {
        boolean goOn;
        do {
            System.out.print("Escribe una palabra que quieras traducir " +
                    "(Escribe 'Finalizar' para finalizar el programa): ");

            String line = bufReadCommLine.readLine();
            goOn = !line.equalsIgnoreCase("finalizar");

            bufWriter.write(line);
            bufWriter.newLine();
            bufWriter.flush();

            if (goOn) {
                String traduccion = bufReader.readLine();
                System.out.println(traduccion);
                bufferedWriter.write(line + " -> " + traduccion);
                bufferedWriter.newLine();
                bufferedWriter.flush();
            }
        } while (goOn);
    }

    /***
     * Este metodo se encarga de realizar la comunicación entre el proceso padre e hijo mediante un argumento el cual
     * será el diccionario que use el proceso hijo
     * @param process Proceso hijo
     * @param traducciones Fichero destino de las traducciones
     * @param diccionarioArg Ruta del archivo que se usará como diccionario
     */
    private static void comunicacionPadreHijoDiccionario(Process process, File traducciones, String diccionarioArg) {
        try (OutputStreamWriter outputStreamWriter = new OutputStreamWriter(process.getOutputStream());
             InputStreamReader inputStreamReader = new InputStreamReader(process.getInputStream());
             BufferedReader bufReader = new BufferedReader(inputStreamReader);
             BufferedReader bufReadCommLine = new BufferedReader(new InputStreamReader(System.in));
             BufferedWriter bufWriter = new BufferedWriter(outputStreamWriter);
             FileWriter fileWriter = new FileWriter(traducciones);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {

            bufWriter.write(diccionarioArg);
            bufWriter.newLine();
            bufWriter.flush();

            buffers(bufReader, bufReadCommLine, bufWriter, bufferedWriter);
        } catch (IOException e){
            System.err.println(1);
            System.out.println(e.getMessage());
        }
    }

    /***
     * Este metodo se encarga de verificar que todo ha salido correctamente
     * @param process Proceso hijo
     * @throws InterruptedException Excepción lanzada si se interrumpe el proceso
     */
    private static void processWaitFor(Process process) throws InterruptedException {
        int exitValue = process.waitFor();
        System.out.println(System.lineSeparator() + "Valor de Salida: " + exitValue);
        System.out.println(System.lineSeparator() + "La traducción se ha guardado en el fichero: " + TRADUCCIONES_FILE);
        if (exitValue != 0) {
            System.err.println("ERROR TRADUCCION");
            printChildError(process.getErrorStream());
        }
    }
}
