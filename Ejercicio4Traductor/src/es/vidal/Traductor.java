package es.vidal;

import java.io.*;
import java.util.HashMap;

public class Traductor {
    public static void main(String[] args) throws IOException {

        BufferedReader buffReader = new BufferedReader(new InputStreamReader(System.in));
        HashMap<String, String> diccionario = new HashMap<>();

        diccionario.put("Hola", "Hi");
        diccionario.put("Adiós", "Bye");
        diccionario.put("Bienvenido", "Welcome");
        diccionario.put("Casa", "House");
        diccionario.put("Suelo", "Floor");
        diccionario.put("Gente", "People");
        diccionario.put("Luz", "Light");
        diccionario.put("Coche", "Car");

        String word;
        boolean goOn = true;
        while (goOn && ((word = buffReader.readLine()) != null)) {
            if (word.equalsIgnoreCase("finalizar")) {
                goOn = false;
            } else if (word.contains("diccionario.txt")){
                File file = new File(word);
                diccionario.clear();
                try (FileReader fileReader = new FileReader(file);
                     BufferedReader bufferedReader = new BufferedReader(fileReader)){

                    String line = bufferedReader.readLine();
                    while (line != null){
                        String[] keyAndValues = line.split(",");
                        diccionario.put(keyAndValues[0].trim(), keyAndValues[1].trim());
                        line = bufferedReader.readLine();
                    }
                }catch (IOException e){
                    System.err.println(2);
                    System.out.println(e.getMessage());
                }
            }else {
                System.out.println(diccionario.getOrDefault(word, "Desconocido"));
            }
        }
    }
}
