package es.vidal;

import java.io.*;

public class Ejercicio5Adder {

    public static void main(String[] args){

        if (args.length > 0){
            sumarNumerosFichero(args[0]);
        }
    }

    /***
     * Este metodo se encarga de sumar todos los números de un fichero
     * @param fichero fichero del cual recibir los valores para sumar
     */
    private static void sumarNumerosFichero(String fichero){

        File file = new File(fichero);

        try (FileReader fileReader = new FileReader(file);
             BufferedReader bufferedReader = new BufferedReader(fileReader)){

            String number = bufferedReader.readLine();
            int total = 0;
            while (number != null){

                total += Integer.parseInt(number);
                number = bufferedReader.readLine();
            }
            System.out.println(total);
        } catch (IOException e){
            System.err.println(1);
            System.out.println(e.getMessage());
        }
    }
}
